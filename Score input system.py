import os
global score
global sort_input
global sortbyscore
global sortbyname
global function

name = {}
def score_input():
  stu_name = raw_input('Please enter a name:\n')
  while True:
    try:
      score = float(raw_input('Please enter the score:\n'))
    except EOFError,e:
      print e
    except IOError,e:
      print e
    except ValueError,e:
      print e
    else:
      if type(score)==type(0.0):
        break
  score=round(score,2)
  name[stu_name] = score
  print '\nSuccess'
  os.system('pause')

def check_score():
  name_get=raw_input('Please enter a name:\n')
  if name.has_key(name_get):
      print 'The score is:',name[name_get]
  else:
      print 'not found :('
  os.system('pause')

def del_score():
  name_get=raw_input('Please enter a name:\n')
  if name.has_key(name_get):
    del name[name_get]
    print 'Success'
  else:
    print 'not found :('
  os.system('pause')

def sort_score():
  print '\n1->sorted by score 2->sorted by name'
  while True:
    sort_input = raw_input()
    if sort_input.isdigit():
      break
    else:
      print '\nPlease enter an available number'
  sort_input=int(sort_input)
  if sort_input==1:
    sortbyscore=sorted(name.iteritems(),key=lambda d:d[1],reverse=True)
    print sortbyscore
  elif sort_input==2:
    sortbyname=sorted(name.iteritems(),key=lambda d:d[0])
    print sortbyname
  else:
    print '\nPlease enter an available number'
    pass


print '\nStu scores input system :) '
while True:
  print '\n1->scores input 2->check scores 3->del scores 4->sort scores 5->quit'
  while True:
    function = raw_input('\nPlease enter a function code name:')
    if function.isdigit():
      break
    else:
      print '\nPlease enter an available number'
  function=int(function)
  if function==1:
      score_input()
  elif function==2:
      check_score()
  elif function==3:
      del_score()
  elif function==4:
      sort_score()
  elif function==5:
      break
  else:
      print '\nPlease enter an available number'
      pass
